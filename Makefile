build:
	bdftopcf big-cursor.bdf > big-cursor.pcf

clean:
	rm -f big-cursor.pcf

.PHONY: build clean
